<?php
#============================ setting ============================#
include_once ('crest/crest.php');
#=================================================================#
if ($_REQUEST['id']) {
	### достаем данные лида и связанные с ним дела ###
	$lead = CRest::call('crm.lead.get', array('id' => $_REQUEST['id']));
	$activity = CRest::call('crm.activity.list', array('filter' => array('OWNER_ID' => $_REQUEST['id'], 'COMPLETED' => 'N')));
	### проверка отвественных за лид и дело ###
	$leadResp = $lead['result']['ASSIGNED_BY_ID'];
	foreach ($activity as $activ) {
		if ($leadResp == $activ['ASSIGNED_BY_ID']) continue;
		$activResp[] = $activ['ID'];
	}
	### завершаем все дела, где ответственные не совпадают с лидом ###
	foreach ($activResp as $id) {
		$dataUp[] = array(
			'method' => 'crm.activity.update',
			'params' => array(
				'fields' => array('COMPLETED' => 'Y')
			)
		);
	}
	$update = CRest::callBatch($dataUp);
}